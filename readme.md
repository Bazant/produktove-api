produktové API
==============

Aplikacia ktora bude sluzit pre spravu produktov ulozenych v relacnej DB. Implemetacia REST API.

**Struktura DB**:
|name  		|type		|
|-----------|-----------|
|id   		|int        |
|name   	|string		|
|price   	|float(10,2)|
|created_at	|datetime	|
|updated_at |datetime	|

**Operacie API**
|typ 	|popis								|
|-------|-----------------------------------|
|POST	|vlozenie noveho produktu           |
|PUT	|aktualizacia produktu podla ID     |
|DELETE |zmazanie produktu podla ID         |
|GET	|nacitanie jedneho produktu podla ID|
|GET	|vratenie vsetkych produktov v DB   |

Komunikacia klient - server prebieha vo formate JSON.

### pre spustenie je potrebne: 

 1. vytvorit DB a naimportovat data, sql file je umiestneny v docs/database.sql
 2. nastavit connection k db (cast database) v app/Config/config.neon
 3. pustit composer install / update

### dostupne endpointy:
|typ   |routa		          |
|------|--------------------------|
|GET   |/api/products             |
|GET   |/api/products/product/{id}|
|PUT   |/api/products/product/{id}|
|DELETE|/api/products/product/{id}|
|POST  |/api/products/product     |

detailnejsi popis je v docs/swagger.yaml

## Rozsirenia
### zabezpecenie API
zabespecnie by som riesil pomocou paramtertu `x-api-key` v hlavicke poziadavku. Je mozne si to otestovat, pripravil som middleware ktory to obsluhuje, jeho funkcnost sa da zapnut odokomentovanim polozky 
`
    middlewares:
    	middlewares:
		- App\Middlewares\ApiKeyAuthenticationMiddleware
`

defaultne je vypnuta (zakomentovana)

### moznosti dokumentacie API
dokumntacia je v docs/swagger.yaml

### navrh verzovania API
vytvoril by som /api/1.0.0/... a verzia by sa navysovala, popripade kratsie a to iba v1, v2, ...

### filtracia zaznamu strankovania pri GET requestoch
uz som ipmplementoval do api, u routy `/api/products` ktora ma nepovinne parametre:
|nazov |typ|example|
|------|---|-------|
|limit |int|10
|offset|int|1
|order |string|id desc
