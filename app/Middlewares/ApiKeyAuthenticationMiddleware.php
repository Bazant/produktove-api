<?php
declare(strict_types=1);

namespace App\Middlewares;

use Apitte\Core\Http\ApiResponse;
use Contributte\Middlewares\IMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use stdClass;

final class ApiKeyAuthenticationMiddleware implements IMiddleware {

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @param callable $next
	 * @return ResponseInterface
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
	{
		$apiKey = $request->getHeader('x-api-key');
		$apiKey = reset($apiKey);
		if ($apiKey === false) {
			return $this->notAuthenticated($response, 'Chýbajuci parameter x-api-key');
		}

		if (!$user = $this->getUser($apiKey)) {
			return $this->notAuthenticated($response, 'Provided apiKey was not found');
		}

		return $next($request->withAttribute('user', $user), $response);
	}

	private function getUser(string $apiKey): ?stdClass
	{
		if ($apiKey !== 'supertajneheslo') {
			return null;
		}

		$user = new stdClass();
		$user->firstname = 'Foo';
		$user->lastname = 'Bar';
		return $user;
	}

	/**
	 * @param ResponseInterface $response
	 * @param string $message
	 * @return ResponseInterface
	 */
	private function notAuthenticated(ResponseInterface $response, string $message): ResponseInterface
	{
		return $response->withStatus(ApiResponse::S401_UNAUTHORIZED)->writeJsonBody([
						'status' => 'error',
						'code' => ApiResponse::S401_UNAUTHORIZED,
						'data' => [],
						'message' => $message
		]);
	}

}
