<?php
declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Utils\Strings;
use Nette\Database\Explorer;
use Nette\Database\Table\Selection;
use Nette\Database\Table\ActiveRow;
use DateTime;

class ProductsRepository {

	use Nette\SmartObject;

	public const
			TABLE_NAME = 'products',
			COLUMN_ID = 'id',
			COLUMN_NAME = 'name',
			COLUMN_PRICE = 'price',
			COLUMN_CREATED_AT = 'created_at',
			COLUMN_UPDATED_AT = 'updated_at';

	private Explorer $database;

	public function __construct(Explorer $database)
	{
		$this->database = $database;
	}
	
	/**
	 * @param int $limit
	 * @param int $offset
	 * @param string $order
	 * @return Selection
	 */
	public function getAll(?int $limit = null, ?int $offset = null, ?string $order = null): Selection
	{
		$selection = $this->database->table(self::TABLE_NAME);
		if(!is_null($limit) && !is_null($offset)){
			$selection->limit($limit, $offset);
		}
		if(!is_null($order)){
			$selection->order(Strings::upper($order));
		}
		return $selection;
	}

	/**
	 * @param int $id
	 * @return Selection
	 */
	public function getById(int $id): Selection
	{
		return $this->database->table(self::TABLE_NAME)->where([
						self::COLUMN_ID => $id
		]);
	}

	/**
	 * @param string $name
	 * @param float $price
	 * @return ActiveRow
	 */
	public function save(string $name, float $price): ActiveRow
	{
		$insert = [
				self::COLUMN_NAME => $name,
				self::COLUMN_PRICE => $price,
				self::COLUMN_CREATED_AT => new DateTime(),
		];
		return $this->database->table(self::TABLE_NAME)->insert($insert);
	}

	/**
	 * @param int $id
	 * @param string $name
	 * @param float $price
	 * @return int
	 */
	public function update(int $id, string $name, float $price): int
	{
		$update = [
				self::COLUMN_NAME => $name,
				self::COLUMN_PRICE => $price,
				self::COLUMN_UPDATED_AT => new DateTime(),
		];
		$product = $this->getById($id);
		return $product->update($update);
	}

	/**
	 * @param int $id
	 * @return int
	 */
	public function delete(int $id): int
	{
		return $this->getById($id)->delete();
	}

}
