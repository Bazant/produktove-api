<?php
declare(strict_types=1);

namespace App\Controllers;

use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\RequestParameter;
use Apitte\Core\Annotation\Controller\RequestParameters;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Nette\Utils\Strings;
use App\Model\ProductsRepository;

/**
 * @Path("/products")
 */
final class ProductsController extends BaseController {

	private ProductsRepository $productRepository;

	public function __construct(ProductsRepository $productRepository)
	{
		$this->productRepository = $productRepository;
	}

	/**
	 * @Path("/")
	 * @Method("GET")
	 * @RequestParameters({
	 * 		@RequestParameter(name="limit", type="int", in="query",required=false, allowEmpty=true, description="limit"),
	 * 		@RequestParameter(name="offset", type="int", in="query",required=false, allowEmpty=true, description="offset"),
	 * 		@RequestParameter(name="order", type="string", in="query",required=false, allowEmpty=true, description="order"),
	 * })
	 */
	public function getAll(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$limit = $request->getParameter('limit');
		$offset = $request->getParameter('offset');
		$order = $request->getParameter('order');
		$data = $this->productRepository->getAll($limit, $offset, $order)->fetchAssoc('id');
		return $response->writeJsonBody(self::responseData($data));
	}

	/**
	 * @Path("/product/{id}")
	 * @Method("GET")
	 * @RequestParameters({
	 * 		@RequestParameter(name="id", type="int",required=true, description="id produktu"),
	 * })
	 */
	public function get(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		dumpe($request->getParameters());
		$data = $this->productRepository->getById($request->getParameter('id'))->fetch()->toArray();
		return $response->writeJsonBody(self::responseData($data));
	}

	/**
	 * @Path("/product")
	 * @Method("POST")
	 */
	public function post(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$post = $request->getParsedBody();
		$errors = [];
		$data = [];
		$message = null;
		$code = ApiResponse::S201_CREATED;

		//povinne polozky
		if (!isset($post['name'])) {
			$errors[] = 'Parameter name je povinný';
		}
		if (!isset($post['price'])) {
			$errors[] = 'Parameter price je povinný';
		}

		if (count($errors) == 0) {
			//max dlzka name je 100
			if (Strings::length($post['name']) <= 100) {
				$product = $this->productRepository->save($post['name'], $post['price']);
				if ($product) {
					$data['id'] = $product->id;
				}
			} else {
				$errors[] = 'Parameter name moze mať maximálnu dĺžku 100 znakov';
			}
		}

		if (count($errors) > 0) {
			$code = ApiResponse::S400_BAD_REQUEST;
			$message = implode(', ', $errors);
		}

		return $response->withStatus($code)->writeJsonBody(self::responseData($data, $code, $message));
	}

	/**
	 * @Path("/product/{id}")
	 * @Method("PUT")
	 * @RequestParameters({
	 * 		@RequestParameter(name="id", type="int", required=true, description="id produktu"),
	 * 		@RequestParameter(name="name", type="string", in="query", required=true, description="nazov produktu"),
	 * 		@RequestParameter(name="price", type="float", in="query", required=true, description="cena produktu"),
	 * })
	 */
	public function put(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$message = null;
		$product = $this->productRepository->update($request->getParameter('id'), $request->getParameter('name'), $request->getParameter('price'));
		if ($product) {
			$message = sprintf('Produkt s id %s bol aktualizovaný', $request->getParameter('id'));
		}

		return $response->writeJsonBody(self::responseData([], ApiResponse::S200_OK, $message));
	}

	/**
	 * @Path("/product/{id}")
	 * @Method("DELETE")
	 * @RequestParameters({
	 * 		@RequestParameter(name="id", type="int", required=true, description="id produktu"),
	 * })
	 */
	public function delete(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		$message = null;
		$product = $this->productRepository->delete($request->getParameter('id'));
		$code = ApiResponse::S200_OK;
		if ($product) {
			$message = 'Produkt bol zmazaný';
		} else {
			$message = sprintf('Záznam s id %s nebol nájdený', $request->getParameter('id'));
			$code = ApiResponse::S404_NOT_FOUND;
		}

		return $response->withStatus($code)->writeJsonBody(self::responseData([], $code, $message));
	}

	/**
	 * @param array $data
	 * @param int $code
	 * @param string|null $message
	 * @return array
	 */
	private static function responseData(array $data, int $code = ApiResponse::S200_OK, ?string $message = null): array
	{
		$status = ($code >= 200 && $code <= 226) ? 'success' : 'error';
		return [
				'status' => $status,
				'code' => $code,
				'data' => $data,
				'message' => $message
		];
	}

}
