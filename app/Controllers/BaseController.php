<?php
declare(strict_types = 1);

namespace App\Controllers;

use Apitte\Core\UI\Controller\IController;
use Apitte\Core\Annotation\Controller\Path;

/**
 * @path("/api")
 */
abstract class BaseController implements IController
{

}
