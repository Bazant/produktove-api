-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `cloudsailor-api` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_czech_ci */;
USE `cloudsailor-api`;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `price` float(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

INSERT INTO `products` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1,	'Položka 1',	10.00,	'2021-03-27 14:12:45',	NULL),
(2,	'Položka 2',	15.00,	'2021-03-27 14:13:05',	NULL),
(3,	'Položka 3',	12.50,	'2021-03-27 14:13:29',	NULL);

-- 2021-03-28 16:38:20